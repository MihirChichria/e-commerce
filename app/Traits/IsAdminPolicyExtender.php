<?php


namespace App\Traits;


use App\Models\User;

trait IsAdminPolicyExtender
{
    public function before(User $user, $ability)
    {
        if($user->isAdmin()) {
            return true;
        }
    }
}
