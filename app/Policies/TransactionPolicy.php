<?php

namespace App\Policies;

use App\Models\Transaction;
use App\Models\User;
use App\Traits\IsAdminPolicyExtender;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;
    use IsAdminPolicyExtender;

    private function isBuyer(User $user, Transaction $transaction): bool
    {
        // Can keep in model!
        return $user->id === $transaction->buyer_id;
    }

    private function isSeller(User $user, Transaction $transaction): bool
    {
        // Can keep in model!
        return $user->id === $transaction->product->seller->id;
    }

    public function view(User $user, Transaction $transaction)
    {
        return $this->isBuyer($user, $transaction) || $this->isSeller($user, $transaction);
    }
}
