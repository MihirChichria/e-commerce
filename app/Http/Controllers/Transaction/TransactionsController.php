<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\ApiController;
use App\Models\Transaction;

class TransactionsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index', 'show');
        $this->middleware('scope:read-general')->only('show');
        $this->middleware('can:view,transaction')->only('show');
    }

    public function index()
    {
        $products = Transaction::all();
        return $this->showAll($products);
    }

    public function show(Transaction $transaction)
    {
        return $this->show($transaction);
    }
}
