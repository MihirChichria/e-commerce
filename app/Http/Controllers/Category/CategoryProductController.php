<?php


namespace App\Http\Controllers\Category;


use App\Http\Controllers\ApiController;
use App\Models\Category;

class CategoryProductController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only('index');
    }

    public function index(Category $category)
    {
        return $this->showAll($category->products);
    }
}
